# Chatty

A simple to use messaging app for 1:1 communication 
and small groups supporting SMS, MMS, Matrix and XMPP.

As of 2023-10-27 Upstream development moved to GNOME's gitlab.
The new location for code and issues is at 
https://gitlab.gnome.org/World/Chatty.

The packaging for the Librem 5 and PureOS lives at
https://source.puri.sm/Librem5/debs/pkg-chatty.
